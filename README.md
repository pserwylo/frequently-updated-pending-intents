We had a problem in the Android F-Droid client ([Issue #652](https://gitlab.com/fdroid/fdroidclient/issues/652)) whereby a notification + progress bar + action button only worked the first time the notification was shown, and the action refused to be pressed after subsequently updating the progress bar. After searching around for some time, trying best to understand how `PendingIntent`s work, and making sure that the relevant flags were being passed to both the inner `Intent` and the `PendingIntent` which wrapped it, I did not have much success. Finally, I created a small test project to try and reproduce the problem without having to worry about a big application and all of the things that can go wrong in that context. This repo is the project which helped me diagnose the problem.

In the interests of preventing others from having to go through the same problem I had, here is a quick run down of the problem:
 * Notifications can get updated throughout their lifetime
 * When updating, as long as the `tag` and `id` are the same as a previous notification posted by your app, then the existing notification will be updated.
 * Our notification was being updated frequently (on a timer every 100ms), in order to keep the notification up to speed with a download.
 * The notification had a cancel action which was added via `NotificationCompat.Builder#addAction`.
 * The notification worked the first time it was shown, before any progress events were received and subsequently updated the notification.
 * After the notification was updated, the action button could no longer be pressed.
 * Some times the button could be pressed, but only very infrequently.

The problem turned out to be:
 * Frequent updating of a notification seems to cause the action button to be unpressable.
 * If the user presses the action button just the right time (I suspect right after it was updated) then it may work.
 * Slowing the rate at which the notification is updated seems to prevent this problem.
 * Going from 100ms to 300ms made it more likely that pressing the action button would work, but there were still many times it would not work.
 * Waiting 500ms between notification updates seemed to be a nice compromise between fast updating notifications, and the ability to actually interact with the notification via an action button.
