package com.serwylo.pendingintent.pendingintentexample;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    /**
     * If this is too low (e.g. 100ms) then the notification updates too frequently. The frequent
     * updating of the Notification makes it so that you are unable to touch the action button on
     * the notification. If it is increased to a greater interval, e.g. 500ms, then touching the
     * action button will almost always trigger the pending intent.
     */
    private final int UPDATE_INTERVAL_MS = 100;

    private final String EXTRA_COUNTER = "COUNTER";
    private final int NOTIFICATION_ID = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        new AsyncTask<Void, Void, Void>() {

            @Override
            protected Void doInBackground(Void... params) {
                NotificationManager manager = (NotificationManager)getSystemService(NOTIFICATION_SERVICE);
                try {

                    // A slow initial sleep to show how there is never any troubles touching the
                    // notification when it is not getting updated. It is only when it is frequently
                    // updated that the problems occur.
                    manager.notify(NOTIFICATION_ID, getNotification(-1).build());
                    Thread.sleep(5000);

                    for (int i = 0; i < 100; i ++) {
                        manager.notify(NOTIFICATION_ID, getNotification(i).setProgress(100, i, false).build());
                        Thread.sleep(UPDATE_INTERVAL_MS);
                    }

                } catch (InterruptedException e) {
                    e.printStackTrace();
                } finally {
                    manager.cancel(NOTIFICATION_ID);
                }
                return null;
            }

        }.execute();
    }

    /**
     * Creates a notification with a {@link PendingIntent} which will launch this activity and
     * tell you how far through the progress bar we've gone.
     */
    private NotificationCompat.Builder getNotification(int counter) {
        Intent intent = new Intent(getApplicationContext(), MainActivity.class);
        intent.putExtra(EXTRA_COUNTER, counter);

        PendingIntent blahPendingIntent = PendingIntent.getActivity(getApplicationContext(), 0, intent, PendingIntent.FLAG_CANCEL_CURRENT);

        return new NotificationCompat.Builder(getApplicationContext())
                .setAutoCancel(false)
                .setOngoing(true)
                .setContentTitle("Doing stuff")
                .addAction(android.R.drawable.ic_menu_info_details, "Blah", blahPendingIntent)
                .setSmallIcon(android.R.drawable.stat_sys_download)
                .setContentText("This is it")
                .setProgress(100, 0, false);
    }

    /**
     * Update the view to indicate to the user how far through the progress counter was when the
     * notification action was pressed.
     */
    @Override
    protected void onResume() {
        super.onResume();
        String text;
        if (!getIntent().hasExtra(EXTRA_COUNTER)) {
            text = "Touch the notifications action button to update this text";
        } else {
            int counter = getIntent().getIntExtra(EXTRA_COUNTER, -1);
            text = counter >= 0 ? "Notification touched when counter @ " + counter : "Notification touched while waiting at the beginning";
        }
        ((TextView)findViewById(R.id.text)).setText(text);
    }
}
